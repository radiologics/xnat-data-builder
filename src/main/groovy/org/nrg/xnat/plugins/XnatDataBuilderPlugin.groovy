/*
 * xnat-data-builder: org.nrg.xnat.plugins.XnatDataBuilderPlugin
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.plugins

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.tasks.bundling.Jar
import org.gradle.api.tasks.compile.JavaCompile
import org.nrg.xdat.tasks.XdatDataBuilderBeanJarTask
import org.nrg.xdat.tasks.XdatDataBuilderGenerateSourceTask
import org.nrg.xnat.tasks.XnatPluginInfoTask

class XnatDataBuilderPlugin implements Plugin<Project> {
    void apply(Project project) {
        final pluginInfo = project.task('xnatPluginInfo', type: XnatPluginInfoTask)
        final dataBuilder = project.task('xnatDataBuilder', type: XdatDataBuilderGenerateSourceTask)
        final beanJar = project.task('xnatBeanJar', type: XdatDataBuilderBeanJarTask)

        // This will eventually kick off the source generation task to pre-populate any code dependencies required.
        project.afterEvaluate {
            project.logger.lifecycle "Completed evaluation of project ${project.name}."
        }

        // Set the primary compileJava task to depend on generate source.
        (project.tasks.getByName("compileJava") as JavaCompile).with {
            dependsOn dataBuilder
            doLast {
                pluginInfo
            }
        }

        // Set the primary jar task to trigger bean jar task.
        (project.tasks.getByName("jar") as Jar).with {
            dependsOn beanJar
            // Add "-plugin" to the output jar name if it's not already there.
            baseName = "${project.name}${project.name.endsWith("-plugin") ? "" : "-plugin"}"
        }
    }
}
