/*
 * xnat-data-builder: org.nrg.xnat.tasks.XnatDataBuilderTaskTest
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.tasks

import org.junit.Test
import org.nrg.xdat.tasks.XdatDataBuilderGenerateSourceTask
import org.nrg.xnat.common.XnatDataBuilderTest

import static org.junit.Assert.assertTrue

class XnatDataBuilderTaskTest extends XnatDataBuilderTest {
    XnatDataBuilderTaskTest() {
        super(false)
    }

    @Test
    public void canAddTaskToProject() {
        def task = project.task('xnatDataBuilder', type: XdatDataBuilderGenerateSourceTask)
        assertTrue(task instanceof XdatDataBuilderGenerateSourceTask)
    }
}
