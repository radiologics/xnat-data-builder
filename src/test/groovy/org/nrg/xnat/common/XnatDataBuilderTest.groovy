/*
 * xnat-data-builder: org.nrg.xnat.common.XnatDataBuilderTest
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.common

import org.gradle.api.plugins.JavaPlugin
import org.gradle.testfixtures.ProjectBuilder
import org.nrg.xnat.plugins.XnatDataBuilderPlugin

class XnatDataBuilderTest {
    def project

    XnatDataBuilderTest(boolean includePlugin = true) {
        project = ProjectBuilder.builder().build()
        project.pluginManager.apply JavaPlugin
        if (includePlugin) {
            project.pluginManager.apply XnatDataBuilderPlugin
        }
    }
}
