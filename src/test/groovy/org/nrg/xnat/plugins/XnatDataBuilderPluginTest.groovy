/*
 * xnat-data-builder: org.nrg.xnat.plugins.XnatDataBuilderPluginTest
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.plugins

import org.junit.Test
import org.nrg.xdat.tasks.XdatDataBuilderGenerateSourceTask
import org.nrg.xnat.common.XnatDataBuilderTest

import static org.junit.Assert.assertTrue

class XnatDataBuilderPluginTest extends XnatDataBuilderTest {
    @Test
    public void xnatDataBuilderPluginAddsXnatDataBuilderTaskToProject() {
        assertTrue(project.tasks.xnatDataBuilder instanceof XdatDataBuilderGenerateSourceTask)
    }
}
